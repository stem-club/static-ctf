const salt = document.querySelector("#salt");
const flag = document.querySelector("#flag");
const output = document.querySelector("#output");

flag.addEventListener("input", () => {
    const toHash = "w@4^36Y&WYwWJr@KzQ$Hh7HFoSmLxeMz" + flag.value + salt.value;
    const hashed = CryptoJS.SHA512(toHash).toString();
    output.innerText = hashed;
});