function handleProbData(probData) {
    const ul = document.getElementById("probs");
    for (const prob of probData) {
        ul.appendChild(createProbLi(prob));
    }
}

function createProbLi(prob) {
    const li = document.createElement("li");

    const title = document.createElement("h2");
    title.innerText = prob["title"];
    li.appendChild(title);

    const author = document.createElement("h3");
    author.innerText = "By " + prob["author"];
    li.appendChild(author);

    const desc = document.createElement("div");
    desc.innerHTML = prob["description"];
    li.appendChild(desc);

    const downloadList = createDownloadList(prob);
    if (downloadList !== null) {
        li.appendChild(downloadList);
    }

    const flagInput = createFlagInput(prob, () => {
        title.innerText += " (Solved)";
    });
    li.appendChild(flagInput);

    return li;
}

function createDownloadList(prob) {
    if (!("files" in prob))
        return null;
    
    const container = document.createElement("div");

    const label = document.createElement("h3");
    label.innerText = "Files";
    container.appendChild(label);

    const ul = document.createElement("ul");
    for (const file of prob["files"]) {
        const li = document.createElement("li");
        li.innerHTML = '<a href="probs/' + prob["id"] + '/' + file + '" download>' + file + "</a>";
        ul.appendChild(li);
    }
    container.appendChild(ul);

    return container;
}

function createFlagInput(prob, onSolved) {
    const container = document.createElement("div");

    const label = document.createElement("h3");
    label.innerText = "Flag";
    container.appendChild(label);

    const input = document.createElement("input");
    container.appendChild(input);

    const check = document.createElement("button");
    check.innerText = "Check";
    container.appendChild(check);

    const result = document.createElement("p");
    container.appendChild(result);

    if (isSolved(prob["id"])) {
        onSolved();
    }

    check.addEventListener("click", () => {
        const toHash = "w@4^36Y&WYwWJr@KzQ$Hh7HFoSmLxeMz" + input.value + prob["flagSalt"];
        const hashed = CryptoJS.SHA512(toHash).toString();
        console.log(toHash, hashed);
        if (hashed === prob["flagHash"]) {
            result.innerText = "Congrats, you got the flag!";
            if (!isSolved(prob["id"])) {
                if ("localStorage" in window) {
                    const solved = JSON.parse(localStorage.getItem("solved"));
                    solved.push(prob["id"]);
                    localStorage.setItem("solved", JSON.stringify(solved));
                }
                onSolved();
            }
        } else {
            result.innerText = "That's not the flag. Keep looking!";
        }
    });

    return container;
}

function isSolved(probId) {
    if ("localStorage" in window) {
        if (localStorage.getItem("solved") === null) {
            localStorage.setItem("solved", "[]");
        }
        const solved = JSON.parse(localStorage.getItem("solved"));
        return solved.includes(probId);
    }
}

function main() {
    const xmlhttp = new XMLHttpRequest();
    const url = "prob.json";

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            const probData = JSON.parse(this.responseText);
            handleProbData(probData);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

main();
